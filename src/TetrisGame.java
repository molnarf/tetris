import java.io.File;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Random;

import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;

public class TetrisGame extends Observable{

	private int height;
	private int width;
	private Brick[][] board;
	private boolean ongoing = false;
	private ArrayList<Brick> aktuelleBloecke;
	private ArrayList<Brick> naechstenBloecke;
	private ArrayList<Brick> davor;
	private boolean fallpaint = false;
	
	
	public TetrisGame(int height, int width){
		this.height = height;
		this.width = width;
		board = new Brick[height][width];
		aktuelleBloecke = new ArrayList<Brick>(4);
		naechstenBloecke = new ArrayList<Brick>(4);
		
		for(int i = 0; i<height; i++){
			for(int j = 0; j<width; j++){
				board[i][j] = new Brick(i,j,Color.WHITE);
			}
		}
		
		aktuelleBloecke = generateFormation();
		naechstenBloecke = generateFormation();
		//reset();
	}
	
	public void reset(){
		this.ongoing = true;
		board = new Brick[height][width];
		for(int i = 0; i<height; i++){
			for(int j = 0; j<width; j++){
				board[i][j] = new Brick(i,j);
			}
		}
		
		setChanged();
		notifyObservers();
	}
	
	public ArrayList<Brick> generateFormation(){
		
		ArrayList<Brick> bloecke = new ArrayList<Brick>(4);
		
		
		Random rd = new Random();
		int farbe = rd.nextInt(7);
		Color color = Color.BLUE;
		if(farbe == 0){
			color = Color.BLUE;
		}
		if(farbe == 1){
			color = Color.GREEN;
		}
		if(farbe == 2){
			color = Color.RED;
		}
		if(farbe == 3){
			color = Color.GOLD;
		}
		if(farbe == 4){
			color = Color.HOTPINK;
		}
		if(farbe == 5){
			color = Color.PURPLE;
		}
		if(farbe == 6){
			color = Color.ORANGE;
		}
				
		int formation = rd.nextInt(7);
		Brick b1 = new Brick(0, width/2);
		Brick b2 = new Brick(0,0);
		Brick b3 = new Brick(0,0);
		Brick b4 = new Brick(0,0);
		b1.setColor(color);
		b2.setColor(color);
		b3.setColor(color);
		b4.setColor(color);
		b1.setBesetzt(true);
		b2.setBesetzt(true);
		b3.setBesetzt(true);
		b4.setBesetzt(true);
		
		if(formation == 0){		//Quadrat
			//		XX
			//		XX
			b2.setX(width/2+1);
			b3.setX(width/2);
			b3.setY(1);
			b4.setX(width/2+1);
			b4.setY(1);
		}
		
		if(formation == 1){		//Linie
			//		XXXX
			b1.setY(0);
			b2.setY(0);
			b3.setY(0);
			b4.setY(0);
			b1.setX(width/2-2);
			b2.setX(width/2-1);
			b3.setX(width/2);
			b4.setX(width/2+1);
		}
		
		if(formation == 2){
			//		X
			//	   XXX
			b2.setX(width/2-1);
			b2.setY(1);
			b3.setX(width/2);
			b3.setY(1);
			b4.setX(width/2+1);
			b4.setY(1);
		}
		
		if(formation == 3){
			//		X
			//	   XX
			//	   X
			b2.setX(width/2);
			b2.setY(1);
			b3.setX(width/2-1);
			b3.setY(1);
			b4.setX(width/2-1);
			b4.setY(2);
		}
		
		if(formation == 4){
			//		X
			//		XX
			//		 X
			b2.setX(width/2);
			b2.setY(1);
			b3.setX(width/2+1);
			b3.setY(1);
			b4.setX(width/2+1);
			b4.setY(2);
		}
		
		if(formation == 5){
			//		X
			//		XXX
			b1.setX(width/2-1);
			b2.setX(width/2-1);
			b2.setY(1);
			b3.setX(width/2);
			b3.setY(1);
			b4.setX(width/2+1);
			b4.setY(1);
		}
		
		if(formation == 6){
			//		XXX
			//		  X
			b1.setX(width/2-1);
			b2.setX(width/2+1);
			b3.setX(width/2);
			b4.setX(width/2+1);
			b4.setY(1);
		}
		
		b1.setY(b1.getY()+1);
		b2.setY(b2.getY()+1);
		b3.setY(b3.getY()+1);
		b4.setY(b4.getY()+1);
		
		bloecke.add(b1);
		bloecke.add(b2);
		bloecke.add(b3);
		bloecke.add(b4);
		
		return bloecke;
	}
	
	public void volleZeile(){
		boolean voll = true;
		for(int i = 0; i<height; i++){
			voll = true;
			for(int j = 0; j<width; j++){
				if(!board[i][j].isBesetzt()){
					voll = false;
					continue;
				}
			}
			
			if(voll){
				for(int j = 0; j<width; j++){
				board[i][j] = new Brick(i,j);
				}
				fall(i);
			}
		}
	}
	
	public boolean angekommen(){
		
		for(Brick block : aktuelleBloecke){
			int x = block.getX();
			int y = block.getY();
			if(y+1 == height){
				return true;
			}
			
			if(board[y+1][x].isBesetzt() && !istinaktuelleBloecke(board[y+1][x])){
				return true;
			}
		}
		
		
		return false;
	}
	
	
	public boolean falle1(){
		
		
		boolean ergebnis = false;
		
		davor = new ArrayList<Brick>();
		for(Brick block : aktuelleBloecke){
			davor.add(new Brick(block.getY(), block.getX(), block.getColor(), block.isBesetzt()));
		}
		
		
		if(angekommen()){
			System.out.println("Angekommen");
			
			aktuelleBloecke = new ArrayList<Brick>();
			
			for(Brick block : naechstenBloecke){
				aktuelleBloecke.add(block);
			}
			naechstenBloecke = generateFormation();
			
			
			
			if(isover(aktuelleBloecke.get(0), aktuelleBloecke.get(1), aktuelleBloecke.get(2), aktuelleBloecke.get(3))){
				System.out.println("Over");
				for(int i = 0; i<height; i++){
					for(int j = 0; j<width; j++){
						board[i][j].setColor(Color.BLACK);
						board[i][j].setBesetzt(true);
					}
				}
				return true;
			}
			
			volleZeile();
			for(Brick block : aktuelleBloecke){
				board[block.getY()][block.getX()].setColor(block.getColor());
				board[block.getY()][block.getX()].setBesetzt(true);
				
			}
			
			//ergebnis = true;
			fallpaint = true;
			setChanged();
			notifyObservers();
			return true;
		}
		
		for(Brick block : aktuelleBloecke){
			int x = block.getX();
			int y = block.getY();
			
			if(y+1 >=height){
				return true;
			}
			
			if(board[y+1][x].isBesetzt()){
				boolean eigener = false;
				for(Brick vgl : aktuelleBloecke){
					if((y+1 == vgl.getY()) && (x == vgl.getX())){
						eigener = true;
					}
				}
				if(!eigener){
					return true;
				}
				
			}
		}
		//return wenn der Stein nicht weiterfallen soll
		
	
		ArrayList<Brick> anzupassendeBloecke = new ArrayList<Brick>();
		
		for(Brick block : aktuelleBloecke){
			
			int x = block.getX();
			int y = block.getY();
			
			anzupassendeBloecke.add(board[y][x]);
			block.setY(y+1);
			board[y+1][x].setBesetzt(true);
			board[y+1][x].setColor(block.getColor());
		}
		
		for(Brick block : anzupassendeBloecke){
			int x = block.getX();
			int y = block.getY();
			
			boolean mussweiswerden = true;
			for(Brick aktuellerBlock : aktuelleBloecke){
				if(aktuellerBlock.getX() == x && aktuellerBlock.getY() ==y){
					mussweiswerden = false;
				}
				
			}
			if(mussweiswerden){
				board[y][x].setColor(Color.WHITE);
				board[y][x].setBesetzt(false);
			}
			
		}

		
		setChanged();
		notifyObservers();
		return ergebnis;
	}
	
	public void falleganz(){
		while(!falle1());
	}
	
	public void passean(ArrayList<Brick> davor){
		
		
		for(Brick block : davor){
			int x = block.getX();
			int y = block.getY();
			
			boolean mussweiswerden = true;
			for(Brick aktuellerBlock : aktuelleBloecke){
				if(aktuellerBlock.getX() == x && aktuellerBlock.getY() ==y){
					mussweiswerden = false;
				}
				
			}
			if(mussweiswerden){
				board[y][x].setColor(Color.WHITE);
				board[y][x].setBesetzt(false);
			}
			
		}
		
		for(Brick aktuellerBlock : aktuelleBloecke){
			int x = aktuellerBlock.getX();
			int y = aktuellerBlock.getY();
			
			board[y][x].setBesetzt(true);
			board[y][x].setColor(aktuellerBlock.getColor());
		}
		
		
	}
	
	public boolean istinListe(Brick brick, ArrayList<Brick> liste){
		
		for(Brick block : liste){
			if(brick.getX() == block.getX() && brick.getY() == block.getY()){
				return true;
			}
		}
		return false;
	}
	
	public boolean istinaktuelleBloecke(Brick brick){
		
		for(Brick block : aktuelleBloecke){
			int x = block.getX();
			int y = block.getY();
			
			if(brick.getX() == x && brick.getY() == y){
				return true;
			}
		}
			
		return false;
	}
	

    public void keyPressed(KeyEvent event){
    	KeyCode key = event.getCode();
    	

		davor = new ArrayList<Brick>();
		for(Brick block : aktuelleBloecke){
			davor.add(new Brick(block.getY(), block.getX(), block.getColor(), block.isBesetzt()));
		}
    	
    	if(key == KeyCode.LEFT){
    		System.out.println("links");
    		links();
    	}
    	
    	if(key == KeyCode.RIGHT){
    		System.out.println("rechts");
    		rechts();
    	}
    	
    	if(key == KeyCode.UP){
    		System.out.println("Rotieren gegen Uhrzeigersinn");
    		rotieren(-1);
    	}
    	
    	if(key == KeyCode.DOWN){
    		//System.out.println("Rotieren im Uhrzeigersinn)");
    		System.out.println("Falle1");
    		falleganz();
    		fallpaint = true;
    	}
    	
    	/*
    	if(key == KeyCode.MINUS){
    		System.out.println("Falleganz");
    		falleganz();
    	}
    	*/
    	setChanged();
    	notifyObservers();
    	
    }
    
    /**
     * 			Muss aufgerufen werden, bevor aktuelleBloecke geaendert werden
     * @return anzupassende Bloecke im Feld
     */
    public ArrayList<Brick> getanzupassendeBloecke(){
    	
    	ArrayList<Brick> anzupassendeBloecke = new ArrayList<Brick>(4);
    	for(Brick block : aktuelleBloecke){
			
			int x = block.getX();
			int y = block.getY();
			anzupassendeBloecke.add(board[y][x]);
		}
    
    	return anzupassendeBloecke;
    }
    
    
    public void links(){
    	
    	for(Brick block : aktuelleBloecke){		//keine OutOfBounds
    		if(block.getX()-1 < 0){
    			return;
    		}
    	}
    	
    	for(Brick block : aktuelleBloecke){
    		int x = block.getX();
    		int y = block.getY();
    		
    		if(!istinaktuelleBloecke(board[y][x-1])){	// wenn links kein aktueller block ist
    			if(board[y][x-1].isBesetzt()){		// wenn links trotzdem besetzt ist
    				return;
    			}
    		}
    	}
    	
    	ArrayList<Brick> anzupassendeBloecke = getanzupassendeBloecke();
		
		
    	
    	for(Brick block : aktuelleBloecke){
    		block.setX(block.getX()-1);
    		
    	}
    	passean(anzupassendeBloecke);

    }
    
    public void rechts(){
    	

    	for(Brick block : aktuelleBloecke){		//keine OutOfBounds
    		if(block.getX()+1 >= width){
    			return;
    		}
    	}
    	
    	for(Brick block : aktuelleBloecke){
    		int x = block.getX();
    		int y = block.getY();
    		
    		if(!istinaktuelleBloecke(board[y][x+1])){	// wenn links kein aktueller block ist
    			if(board[y][x+1].isBesetzt()){		// wenn links trotzdem besetzt ist
    				return;
    			}
    		}
    	}
    	
    	ArrayList<Brick> anzupassendeBloecke = getanzupassendeBloecke();
		
		
    	
    	for(Brick block : aktuelleBloecke){
    		block.setX(block.getX()+1);
    		
    	}
    	passean(anzupassendeBloecke);
    }
    
	
	public void fall(int zeile){		
		
		fallpaint = true;
		
        String bip = "C:/Users/Florian/Music/Music/Skrraa.mp3";
        Media hit = new Media(new File(bip).toURI().toString());
        MediaPlayer mediaPlayer = new MediaPlayer(hit);
        mediaPlayer.play();
		
		for(int i = zeile; i>0; i--){
			Brick[] temp = new Brick[width]; //	Array von der Zeile drueber
			for(int j = 0; j<width; j++){
				temp[j] = new Brick(i-1, j, board[i-1][j].getColor(), board[i-1][j].isBesetzt());
			}
			
			
			boolean allwhite = true;
			for(Brick block : temp){
				if(block.getColor() != Color.WHITE){
					allwhite = false;
				}
				if(!istinaktuelleBloecke(block)){
				board[i][block.getX()].setColor(block.getColor());
				board[i][block.getX()].setBesetzt(block.isBesetzt());
				}
			}
			if(allwhite){
				return;
			}
		}
		
		
		
		setChanged();
		notifyObservers();
	}
	
	public void rotieren(int richtung){		//-1 gegen Uhrzeiger	+1 im Uhrzeiger
		
		int b3x = aktuelleBloecke.get(2).getX();
		int b3y = aktuelleBloecke.get(2).getY();
		
		ArrayList<Brick> vorher = new ArrayList<Brick>();
		for(Brick block : aktuelleBloecke){
			vorher.add(new Brick(block.getY(), block.getX(), block.getColor(), block.isBesetzt()));
		}
		
		ArrayList<Brick> anzupassendeBloecke = getanzupassendeBloecke();
		
		
		if(richtung < 0){		// gegen Uhrzeiger
			
			for(Brick block : aktuelleBloecke){
				
					
					//links davon
					if(block.getX() == b3x-1){
						if(block.getY() == b3y-1){	//links oben
							block.setY(b3y+1);
						}
						else if(block.getY() == b3y){	// links
							block.setX(b3x);
							block.setY(b3y+1);
						}
						else if(block.getY() == b3y+1){	// links unten
							block.setX(b3x+1);
							block.setY(b3y+1);
						}
					}
					
					else if(block.getX() == b3x+1){
						if(block.getY() == b3y-1){	//rechts oben
							block.setX(b3x-1);
						}
						else if(block.getY() == b3y){	// rechts
							block.setX(b3x);
							block.setY(b3y-1);
						}
						else if(block.getY() == b3y+1){	// rechts unten
							block.setX(b3x+1);
							block.setY(b3y-1);
						}

					}
					
					else if(block.getX() == b3x){
						if(block.getY() == b3y-1){	//oben
							block.setX(b3x-1);
							block.setY(b3y);
						}
						else if(block.getY() == b3y+1){	// unten
							block.setX(b3x+1);
							block.setY(b3y);
						}
					}
					
					if(block.getX() == b3x-2){
						block.setX(b3x);
						block.setY(b3y+2);
					}
					else if(block.getX() == b3x+2){
						block.setX(b3x);
						block.setY(b3y-2);
					}
					else if(block.getY() == b3y-2){
						block.setX(b3x-2);
						block.setY(b3y);
					}
					else if(block.getY() == b3y+2){
						block.setX(b3x+2);
						block.setY(b3y);
					}
				}
			}
			
		for(Brick block : aktuelleBloecke){
			int x = block.getX();
			int y = block.getY();
			
			if(x>=width || x<0 || y>= height || y<0){
				
				for(int i = 0; i<aktuelleBloecke.size(); i++){
					aktuelleBloecke.set(i, vorher.get(i));
				}
				
				return;
			}
			
			
			if(board[y][x].isBesetzt() && !istinListe(block,vorher)){
				System.out.println("X : " + x + "    Y : " +y);
				for(int i = 0; i<aktuelleBloecke.size(); i++){
					aktuelleBloecke.set(i, vorher.get(i));
				}				
				return;
			}
		}
	
		passean(anzupassendeBloecke);

	}
	
	public boolean isover(Brick b1, Brick b2, Brick b3, Brick b4){

				if(board[b1.getY()][b1.getX()].isBesetzt()){
					return true;
				}
				if(board[b2.getY()][b2.getX()].isBesetzt()){
					return true;
				}
				if(board[b3.getY()][b3.getX()].isBesetzt()){
					return true;
				}
				if(board[b4.getY()][b4.getX()].isBesetzt()){
					return true;

		}
		
		return false;
	}
	
	public Brick[][] getBoard(){
		return this.board;
	}
	
	public int getHeight(){
		return this.height;
	}
	
	public ArrayList<Brick> getdavor(){
		return this.davor;
	}
	
	public ArrayList<Brick> getAktuelleBloecke(){
		return this.aktuelleBloecke;
	}
	
	public ArrayList<Brick> getnaechstenBloecke(){
		return this.naechstenBloecke;
	}
	
	public void setAktuelleBloecke(ArrayList<Brick> liste){
		this.aktuelleBloecke = liste;
	}
	
	public int getWidth(){
		return this.width;
	}
	
	public boolean isOngoing(){
		return ongoing;
	}
	
	public boolean getfallpaint(){
		return this.fallpaint;
	}
	
	public void setfallpaint(boolean x){
		fallpaint = x;
	}
	
	
}
