import javafx.scene.paint.Color;

public class Brick {

	private int x;
	private int y;
	private Color color;
	private boolean besetzt;
	
	public Brick(){
		this.x = 0;
		this.y = 0;
		this.besetzt = false;
		this.color = Color.WHITE;
	}
	
	/**
	 * 
	 * @param y y Koordinate
	 * @param x x Koordinate
	 */
	public Brick(int y, int x){
		this.x = x;
		this.y = y;
	}
	
	public Brick(int y, int x, Color farbe){
		this.x = x;
		this.y = y;
		color = farbe;
	}
	
	public Brick(int y, int x, Color farbe, boolean besetzt){
		this.x = x;
		this.y = y;
		color = farbe;
		this.besetzt = besetzt;
	}
	
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public boolean isBesetzt() {
		return besetzt;
	}

	public void setBesetzt(boolean besetzt) {
		this.besetzt = besetzt;
	}
}
