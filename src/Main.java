import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

/**
 * Controller, Hauptklasse
 */
public class Main extends Application {
	
	private TetrisView tetview;

    @Override
    public void start(Stage primaryStage) throws Exception{
        TetrisGame game = new TetrisGame(20, 10);
        TetrisView view = new TetrisView(game);
        tetview = view;
        Scene scene = new Scene(view);
        scene.setOnKeyPressed(new TAdapter());

        primaryStage.setTitle("Tetris");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    

    public static void main(String[] args) {
        launch(args);
    }

    private class TAdapter implements javafx.event.EventHandler<KeyEvent>{

		@Override
		public void handle(KeyEvent event) {
			// TODO Auto-generated method stub
			if(event.getEventType() == KeyEvent.KEY_PRESSED){
				tetview.getSpiel().keyPressed(event);
			}
		}
		
	}

}
