import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import java.io.File;
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.animation.RotateTransition;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;


/**
 * View
 */
public class TetrisView extends GridPane {

    public static final int scale = 30;
    public static final int DELAY = 100;
	public static final int PERIOD = 100;

	private TetrisGame spiel;
	private boolean ingame;
	private Timer timer;
	private Label label;
	private GridPane feld;
	private GridPane naechsterBlock;
	private RotateTransition rotate;
	private TranslateTransition translate;
	private MediaPlayer mediaPlayer;
	private ArrayList<Media> playlist;
	
	
    public TetrisView(TetrisGame game) {
        super();
        // Board
        spiel = game;
        feld = new GridPane();
        naechsterBlock = new GridPane();
        spiel.addObserver((o,arg) -> {	
        //   PaintComponent();
        	paint(spiel.getdavor(), spiel.getAktuelleBloecke());
        });
        label = new Label ("GAME OVER");
        
        Button button = new Button("Shuffle");
        button.setOnAction(event ->{
        	shuffle();
        });
        
        
        this.add(feld, 0, 0);
        this.add(naechsterBlock, 1,0);
        this.add(button, 1, 0);
      
        
        //music
        playlist = new ArrayList<Media>();
        createPlaylist();
        
        String bip = "C:/Users/Florian/Music/Music/TetrisRemix.mp3";
        Media hit = new Media(new File(bip).toURI().toString());
        mediaPlayer = new MediaPlayer(hit);
        //mediaPlayer.play();F


        
        timer = new Timer();
		timer.scheduleAtFixedRate(new ScheduleTask(), 1000, 400);

        ingame = true;
      
       PaintComponent();
    }
    


    public TetrisGame getSpiel() {
		return spiel;
	}



	public void setSpiel(TetrisGame spiel) {
		this.spiel = spiel;
	}


	public void paint(ArrayList<Brick> davor, ArrayList<Brick> danach){
		
		if(spiel.getBoard()[0][0].getColor() == Color.BLACK || spiel.getfallpaint()){
			PaintComponent();
		}
		
		else{
			
		if(!spiel.angekommen()){
		for(Brick block : davor){
			Rectangle rect = new Rectangle(block.getY(), block.getX(), scale, scale);
			rect.setFill(Color.WHITE);
			rect.setStroke(Color.BLACK);
			feld.add(rect, block.getX(), block.getY());
		}
		}
		for(Brick block : danach){
			Rectangle rect = new Rectangle(block.getY(), block.getX(), scale, scale);
			rect.setFill(block.getColor());
			rect.setStroke(Color.BLACK);
			feld.add(rect, block.getX(), block.getY());
		}
		
		if(spiel.angekommen()){
			PaintComponent();
		}
		}
	}
	
	
	public void PaintComponent(){
		
		spiel.setfallpaint(false);
    	
    	for(int i = 0; i<spiel.getHeight(); i++){
     	   for(int j = 0; j<spiel.getWidth(); j++){
     		   Rectangle rect = new Rectangle(i, j, scale, scale);
     		   rect.setFill(spiel.getBoard()[i][j].getColor());
     		   rect.setStroke(Color.BLACK);
     		   feld.add(rect, j, i);
     	   }
        }
    	
    	if(spiel.getBoard()[0][0].getColor() == Color.BLACK && !(label.getMinWidth() == 100)){

    		
		label.setMinWidth(100);
		label.setMaxWidth(100);
		label.setLayoutX(800);
		label.setLayoutY(750);
		label.setTextFill(Color.YELLOW);
        							
		rotate = new RotateTransition(Duration.seconds(4), label);
        rotate.setFromAngle(0);
        rotate.setToAngle(720);
        rotate.setCycleCount(Timeline.INDEFINITE);
        rotate.setAutoReverse(true);
        rotate.play();
        
        translate = new TranslateTransition(Duration.seconds(4), label);
        translate.setFromX(0);
        translate.setToX(240);
        translate.setCycleCount(Timeline.INDEFINITE);
        translate.setAutoReverse(true);
        translate.play();
        
    	this.add(label,0,0);
    	}

    	for(int i = 0; i<6;i++){
    		for(int j = 0; j<6; j++){
    			Rectangle rect = new Rectangle(i, j, scale, scale);
    			rect.setFill(Color.WHITE);
    			rect.setStroke(Color.WHITE);
    			naechsterBlock.add(rect, i,j);
    		}
    	}
    	for(Brick block : spiel.getnaechstenBloecke()){
    		Rectangle rect = new Rectangle(block.getY(),block.getX(), scale, scale);
    		rect.setFill(block.getColor());
    		rect.setStroke(Color.BLACK);
    		
    		naechsterBlock.add(rect, block.getX()-2, block.getY());
    	}
    	
    	if(spiel.getBoard()[0][0].getColor() == (Color.BLACK)){
    		for(Brick block : spiel.getnaechstenBloecke()){
        		Rectangle rect = new Rectangle(block.getY(),block.getX(), scale, scale);
        		rect.setFill(Color.WHITE);
        		rect.setStroke(Color.WHITE);
        		
        		naechsterBlock.add(rect, block.getX()-2, block.getY());
        		
        		
        		//Music
        		mediaPlayer.stop();
        		String bip = "C:/Users/Florian/Music/Music/Big enough screaming.mp3";
        	    Media hit = new Media(new File(bip).toURI().toString());
        	    mediaPlayer = new MediaPlayer(hit);
        	    mediaPlayer.play();
        	}
		}
    	
    }
    
    private class ScheduleTask extends TimerTask{

		@Override
		public void run() {
			Platform.runLater(() ->{
			
			spiel.falle1();
			if(spiel.getBoard()[0][0].getColor() == (Color.BLACK)){
				PaintComponent();
				timer.cancel();
			}
						
			});
			
		}	
	}
    
    public void createPlaylist(){
    	String bip = "C:/Users/Florian/Music/Music/Tetris.mp3";
    	playlist.add(new Media(new File(bip).toURI().toString()));
    	bip = "C:/Users/Florian/Music/Music/Genesis - Invisible Touch.mp3";
    	playlist.add(new Media(new File(bip).toURI().toString()));
    	bip = "C:/Users/Florian/Music/Music/Pussy.mp3";
    	playlist.add(new Media(new File(bip).toURI().toString()));
    	bip = "C:/Users/Florian/Music/Music/Sido - Kebab.mp3";
    	playlist.add(new Media(new File(bip).toURI().toString()));
    	bip = "C:/Users/Florian/Music/Music/00 Schneider.mp3";
    	playlist.add(new Media(new File(bip).toURI().toString()));
    	bip = "C:/Users/Florian/Music/Music/Eagle Eye Cherry - Save tonight.mp3";
    	playlist.add(new Media(new File(bip).toURI().toString()));
    	bip = "C:/Users/Florian/Music/Music/Big Shaq - The Ting Goes Da Ba Dee Da Ba Die.mp3";
    	playlist.add(new Media(new File(bip).toURI().toString()));
    	bip = "C:/Users/Florian/Music/Music/Eric Prydz - Call On Me.mp3";
    	playlist.add(new Media(new File(bip).toURI().toString()));
    	bip = "C:/Users/Florian/Music/Music/M83 - Midnight City.mp3";
    	playlist.add(new Media(new File(bip).toURI().toString()));
    	bip = "C:/Users/Florian/Music/Music/Demons - Imagine Dragons.mp3";
    	playlist.add(new Media(new File(bip).toURI().toString()));
    	

    	

    }
    
    public void shuffle(){
    	 mediaPlayer.stop();
    	 Random rd = new Random();
    	 int x = rd.nextInt(playlist.size());
    	 mediaPlayer = new MediaPlayer(playlist.get(x));
         mediaPlayer.play();
    	
    }
    
}